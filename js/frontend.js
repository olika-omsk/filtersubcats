/**
 * User: Echo-company
 * Email: info@echo-company.ru
 * Site: https://www.echo-company.ru
 */
$(document).ready(function () {

    // console.log('555555');
    var cat_id = 0;

    $.get('filtersubcats/subcategories',{id_category: 0},function(response){
        // console.log("response:");
        // console.log(response);
        var res = response.data.result;
        var key, sub;
        $('select[name="filter-mark"] option').remove();
        $('select[name="filter-mark"]').append("<option value='0'>Выберите марку...</option>");
        $('select[name="filter-model"]').attr('disabled', 'disabled');
        $('select[name="filter-series"]').attr('disabled', 'disabled');
        $('button.subcat-filters__button').attr('disabled', 'disabled');

        //заполняем первый выпадающий список - марки машин
        if(Object.keys(res).length != 0) {
            for (key in res) {
                sub = res[key];
                if (sub['parent_id'] == 0) {
                    $('select[name="filter-mark"]').append("<option value=" + sub['id'] + ">" + sub['name'] + "</option>");
                }
            }
        }
        else{
            console.log("NO CATEGORIES");
        }
    });

    //выбрана марка машины в первом выпадающем списке
    $('select[name="filter-mark"]').change( function() {
        var $that = $(this);
        filtermark = $that.val();
        cat_id = filtermark;

        $('div.subcat-prodlist ul').remove();

        // console.log("filtermark:");
        // console.log(filtermark);

        //получаем подкатегории выбранной марки - модели авто
        $.get('filtersubcats/subcategories',{id_category: filtermark},function(response){
            var res = response.data.result;
            var key, sub;

            $('select[name="filter-model"] option').remove();
            $('select[name="filter-model"]').append("<option value='0'>Выберите модель авто...</option>");
            $('select[name="filter-series"] option').remove();
            $('select[name="filter-series"]').append("<option value='0'>Выберите серию...</option>");
            $('select[name="filter-series"]').attr('disabled', 'disabled');
            $('button.subcat-filters__button').attr('disabled', 'disabled');

            //заполняем второй выпадающий список - модели для выбранной марки машин
            if(Object.keys(res).length != 0) {
                if (filtermark != 0) {
                    $('select[name="filter-model"]').removeAttr('disabled');
                    for (key in res) {
                        sub = res[key];
                        if (sub['parent_id'] == filtermark) {
                            $('select[name="filter-model"]').append("<option value=" + sub['id'] + ">" + sub['name'] + "</option>");
                        }
                    }
                }
                else {
                    //выбран нулевой пункт списка "выберите марку..."
                    $('select[name="filter-model"]').attr('disabled', 'disabled');
                    $('select[name="filter-series"]').attr('disabled', 'disabled');
                    $('button.subcat-filters__button').attr('disabled','disabled');
                }
            }
            else{
                //нет дальше подкатегорий, выводим товары по данному id
                $('button.subcat-filters__button').removeAttr('disabled');
                $('select[name="filter-model"]').attr('disabled', 'disabled');
            }
        });
    });

    //выбрана модель машины во втором выпадающем списке
    $('select[name="filter-model"]').change( function() {
        var $that = $(this);
        filtermodel = $that.val();
        cat_id = filtermodel;

        // console.log("filtermodel:");
        // console.log(filtermodel);

        $('div.subcat-prodlist ul').remove();

        //получаем подкатегории выбранной модели - серии авто
        $.get('filtersubcats/subcategories',{id_category: filtermodel},function(response){
            var res = response.data.result;
            var key, sub;

            $('select[name="filter-series"] option').remove();
            $('select[name="filter-series"]').append("<option value='0'>Выберите серию...</option>");
            $('button.subcat-filters__button').attr('disabled', 'disabled');

            //заполняем третий выпадающий список - серии для выбранной модели машин
            if(Object.keys(res).length != 0) {
                if (filtermodel != 0) {
                    $('select[name="filter-series"]').removeAttr('disabled');
                    for (key in res) {
                        sub = res[key];
                        if (sub['parent_id'] == filtermodel) {
                            $('select[name="filter-series"]').append("<option value=" + sub['id'] + ">" + sub['name'] + "</option>");
                        }
                    }
                }
                else {
                    //выбран нулевой пункт списка "выберите модель авто..."
                    $('select[name="filter-series"]').attr('disabled', 'disabled');
                    $('button.subcat-filters__button').attr('disabled','disabled');
                }
            }else {
                //нет дальше подкатегорий, выводим товары по данному id
                $('button.subcat-filters__button').removeAttr('disabled');
                $('select[name="filter-series"]').attr('disabled', 'disabled');
            }
        });
    });

    //выбрана серия машины в третьем выпадающем списке
    $('select[name="filter-series"]').change( function() {
        // console.log("SERIES?");
        var $that = $(this);
        filterseries = $that.val();
        cat_id = filterseries;
        // console.log(filterseries);

        $('div.subcat-prodlist ul').remove();

        //если выбрана серия, активируем кнопку "Подобрать магнитолу"
        if (filterseries != 0) {
            $('button.subcat-filters__button').removeAttr('disabled');
        }
        else{
            //выбран нулевой пункт списка "выберите серию..."
            $('button.subcat-filters__button').attr('disabled','disabled');
        }
    });

    //нажали на кнопку "Подобрать магнитолу", выводим плитку товаров
    $('button.subcat-filters__button').click( function() {
        // console.log("BUTTON!!!");
        // console.log(cat_id);

        var cur_cat = cat_id;

        /* cur_cat - id-номер последней выделеннной категории на момент нажатия на кнопку. Если активен последний выпадающий список -
        то id-номер выбранной серии авто, если последний активный список - второй, то id-номер выбранной модели, а если у выбранной
        в первом списке марки нет подкатегорий, то это id-номер категории этой марки авто */
        if(!($('select[name="filter-series"]').attr('disabled'))){
            // console.log("SERIES");
            cur_cat = $('select[name="filter-series"]').val();
        }
        else if(!($('select[name="filter-model"]').attr('disabled'))){
            // console.log("MODELS");
            cur_cat = $('select[name="filter-model"]').val();
        }
        else {
            // console.log("MARK");
            cur_cat = $('select[name="filter-mark"]').val();
        }

        if(cur_cat == cat_id) {
            // console.log("before get");

            //получаем список всех товаров выбранной категории
            $.get('filtersubcats/allproducts',{id_category: cat_id},function(response){
                // console.log("RESPONSE:");
                var res = response.data.result;
                var pr, im, last;
                // console.log(response);
                // console.log("LENGTH:");
                // console.log(Object.keys(res).length);

                if (Object.keys(res).length != 0) {
                    $('div.subcat-prodlist ul').remove();
                    $('div#prodlist').append('<ul class="product-list thumbs list_thumb_static fixed_height flex flex-wrap flex-justify-center">');

                    for (key in res) {
                        pr = res[key];
                        if (pr['status'] == 1) {
                            // console.log(pr);

                            im = '';
                            last = 100;
                            images1 = pr['img'];
                            for (ind in images1){
                                // console.log(images1[ind]['sort']);
                                if(+images1[ind]['sort'] < +last) {
                                    im = images1[ind]['url_200'];
                                    last = +images1[ind]['sort'];
                                    // console.log("last now: "+last);
                                }
                            }
                            // console.log("IM:");
                            // console.log(im);
                            $('div#prodlist ul').append('<li itemscope="" itemtype="http://schema.org/Product" class="flexdiscount-product-wrap salesku_plugin-product view_class col-md-4 col-sm-6 col-xs-12 li-' + key + '" data-cat="cat_id1066" style="margin: 10px 0;" data-count-md="0" data-count-sm="0">');
                            $('div#prodlist ul li.li-' + key + '').append('<div class="product noshadow animated_shadow new" style="margin: 0 10px;">');
                            $('div#prodlist ul li.li-' + key + ' .animated_shadow').append('<div class="row" style="position: relative;">');
                            $('div#prodlist ul li.li-' + key + ' .animated_shadow .row').append('<form class="purchase addtocart flexdiscount-product-form flying jumping flash" method="post" action="/cart/add/">');

                            if((pr['compare_price'] != 0) && (pr['compare_price'] != pr['price'])) {
                                $('div#prodlist ul li.li-' + key + ' .animated_shadow .row form').append('<a href="' + pr['url'] + '" ><div class="corner top left badge-transparent"><div class="badge low-price"><span>Скидка!</span></div></div></a>');
                            }

                            if(pr['price'] == 0) {
                                $('div#prodlist ul li.li-' + key + ' .animated_shadow .row form').append('<a href="' + pr['url'] + '" ><div class="corner top left badge-transparent"><div class="badge" style="background-color: #a1fcff;"><span>Ожидается</span></div></div></a>');
                            }

                            if(pr['badge']) {
                                if (pr['badge'] != "new") {
                                    $('div#prodlist ul li.li-' + key + ' .animated_shadow .row form').append('<a href="' + pr['url'] + '" ><div class="corner top left badge-transparent">' + pr['badge'] + '</div></a>');
                                }else{
                                    $('div#prodlist ul li.li-' + key + ' .animated_shadow .row form').append('<a href="' + pr['url'] + '" ><div class="corner top left badge-transparent"><div class="badge new"><span>New!</span></div></div></a>');
                                }
                            }

                            if(pr['count'] == 0){
                                $('div#prodlist ul li.li-' + key + ' .animated_shadow .row form').append('<a href="\' + pr[\'url\'] + \'" ><div class="corner top right _badge-transparent" style="right:0;top:29px;"><div class="badge outofstock"><span>Под заказ</span></div></div></a>');
                            }

                            $('div#prodlist ul li.li-' + key + ' .animated_shadow .row form').append('<div class="align-center image_holder" style="position: relative; overflow: hidden; margin: 27px -1px 0;">');
                            $('div#prodlist ul li.li-' + key + ' .animated_shadow .row form .image_holder').append('<div class="valign enlarge">');

                            $('div#prodlist ul li.li-' + key + ' .animated_shadow .row form .image_holder .enlarge').append('<div class="subcat-prod-wrap"><a href="' + pr['url'] + '" ><img src="' + im + '"/></a></div>');

                            if(pr['rating'] > 3) {
                                $('div#prodlist ul li.li-' + key + ' .animated_shadow .row form').append('<div class="rating nowrap align-center" style="height: 18px;">');
                                $('div#prodlist ul li.li-' + key + ' .animated_shadow .row form .rating').append(pr['rating_html']);
                                $('div#prodlist ul li.li-' + key + ' .animated_shadow .row form .rating').append('<a href="' + pr['url'] + '/reviews/" style="line-height:18px;">(' + pr['rating_count'] + ')</a>');
                            }
                            $('div#prodlist ul li.li-' + key + ' .animated_shadow .row form').append('<div class="product_content"><div class="border_holder">');

                            $('div#prodlist ul li.li-' + key + ' .animated_shadow .row form .border_holder').append('<div class="subcat-prod-wrap"><a href="' + pr['url'] + '" >' + pr['name'] + "</a></div>");
                            $('div#prodlist ul li.li-' + key + ' .animated_shadow .row form .product_content').append('<div class="product_offers"></div>');
                            $('div#prodlist ul li.li-' + key + ' .animated_shadow .row form .product_content .product_offers').append('<div class="pricing align-center" style="border-top: none;">');

                            if((pr['compare_price'] != 0) && (pr['compare_price'] != pr['price'])) {
                                $('div#prodlist ul li.li-' + key + ' .animated_shadow .row form .product_content .product_offers .pricing').append('<span class="compare-at-price nowrap">' + pr['compare_price'].toLocaleString("ru-RU",{style:'currency', currency:'RUB', minimumFractionDigits:0, maximumFractionDigits:0}) + '  ');
                            }

                            if(pr['price'] != 0) {
                                $('div#prodlist ul li.li-' + key + ' .animated_shadow .row form .product_content .product_offers .pricing').append('<span class="price nowrap">' + pr['price'].toLocaleString("ru-RU", {
                                    style: 'currency',
                                    currency: 'RUB',
                                    minimumFractionDigits: 0,
                                    maximumFractionDigits: 0
                                }));
                            }
                            else{
                                $('div#prodlist ul li.li-' + key + ' .animated_shadow .row form .product_content .product_offers .pricing').append('<span class="price nowrap">По запросу</span>');
                            }

                            $('div#prodlist ul li.li-' + key + ' .animated_shadow .row form .product_content .product_offers').append('<div itemprop="offers" class="offers" itemscope="" itemtype="http://schema.org/Offer"><input type="hidden" name="product_id" value="' + pr['id'] + '"><div class="xs-align-center">');

                            if(pr['count'] != 0) {
                                $('div#prodlist ul li.li-' + key + ' .animated_shadow .row form .product_content .product_offers .xs-align-center').append('<button type="button" onclick="window.location.href=\'' + pr['url'] + '\';" class="button button100 nowrap"><i class="extra-icon-cart4"></i><span>Подробнее</span></button>');
                            }
                            else{
                                $('div#prodlist ul li.li-' + key + ' .animated_shadow .row form .product_content .product_offers .xs-align-center').append('<button type="submit" class="button button100 nowrap"><i class="extra-icon-cart4"></i><span>Предзаказ</span></button><i class="adding2cart"></i>');
                            }

                            $('div#prodlist ul li.li-' + key + ' .animated_shadow .row form').append('<div class="list_buttons">');
                            $('div#prodlist ul li.li-' + key + ' .animated_shadow .row form .list_buttons').append('<a data-product="' + pr['id'] + '" class="tooltip-btn compare list_store hidden-xs  tooltipstered" href="#" rel="nofollow" data-title="Сравнить" data-clone-title="Не сравнивать"><i class="extra-icon-browsers-o"></i></a>');
                            $('div#prodlist ul li.li-' + key + ' .animated_shadow .row form .list_buttons').append('<a data-product="' + pr['id'] + '" class="tooltip-btn list_store fav tooltipstered" href="#" rel="nofollow" data-title="Нравится!" data-clone-title="Не нравится!"><i class="extra-icon-heart-small-o"></i></a>');
                        }
                    }
                    $('div.subcat-prodlist').css('display', 'block');
                }

            });
        }
        else{
            console.log("ALREADY PRESSED");
        }
    });
});

