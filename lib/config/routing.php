<?php
/**
 * User: Echo-company
 * Email: info@echo-company.ru
 * Site: https://www.echo-company.ru
 */
return array(
    'filtersubcats/subcategories' => array('plugin'=>'filtersubcats', 'module'=>'frontend', 'action'=>'subcategories'),
    'filtersubcats/allproducts' => array('plugin'=>'filtersubcats', 'module'=>'frontend', 'action'=>'allproducts')
);