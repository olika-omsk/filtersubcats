<?php
/**
 * User: Echo-company
 * Email: info@echo-company.ru
 * Site: https://www.echo-company.ru
 */
return array(
    'name' => 'Вывод подкатегорий',
    'description' => 'При выборе в выпадающем списке одной из категорий, выводит в другом выпадающем списке её подкатегории',
    'version' => '1.0',
    'img' => 'img/plugin_icon.png',
    'frontend' => true,
//    'custom_settings' => true,
    'handlers' => array(
        'frontend_head' => 'frontendHead'
    ),
);
