<?php
/**
 * User: Echo-company
 * Email: info@echo-company.ru
 * Site: https://www.echo-company.ru
 */

//class shopFiltersubcatsPlugin extends shopPlugin
class shopFiltersubcatsPlugin extends waPlugin
{
    public function frontendHead()
    {
        return "<link href='/wa-apps/shop/plugins/filtersubcats/css/frontend.css' rel='stylesheet'><script src='/wa-apps/shop/plugins/filtersubcats/js/frontend.js'></script>";
    }

    /*Для вывода на витрине необходимо вызывать его в коде тем дизайна следующим образом:
{if ($wa->shop and class_exists('shopFiltersubcatsPlugin'))}{shopFiltersubcatsPlugin::getAllCategories()}{/if}*/

    public static function getAllCategories()
    {
        $view = wa()->getView();
        $template_file = wa()->getAppPath('plugins/filtersubcats/templates/actions/frontend/template.html', 'shop');

        //Рендерим шаблон
        $html = $view->fetch($template_file);
        return $html;
    }
}
