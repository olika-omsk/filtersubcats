<?php
    /**
     * User: Echo-company
     * Email: info@echo-company.ru
     * Site: https://www.echo-company.ru
     */

class shopFiltersubcatsPluginFrontendSubcategoriesController extends waJsonController
{
    public function execute()
    {
        $id_category = waRequest::get('id_category', 0, waRequest::TYPE_STRING);

        $system = wa('shop');
        $helper = new shopViewHelper($system);

        $subcategories = $helper->categories($id_category);

        $this->response = array(
            'result' => $subcategories //возвращаем в ответ на AJAX-запрос массив подкатегорий данной категории
        );
    }
}