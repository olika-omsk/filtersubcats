<?php
    /**
     * User: Echo-company
     * Email: info@echo-company.ru
     * Site: https://www.echo-company.ru
     */

class shopFiltersubcatsPluginFrontendAllproductsController extends waJsonController
{
    public function execute()
    {
        $id_category = waRequest::get('id_category', 0, waRequest::TYPE_STRING);

        $system = wa('shop');
        $helper = new shopViewHelper($system);
        $products = $helper->products("category/$id_category");

        $products2 = [];
        $images_model = new shopProductImagesModel();

        foreach ($products as $p){
            $images = $images_model->getImages($p['id'], 200, 'id', false);
            $p['img'] = $images;
            $products2.array_push($products2,$p);
        }


        $this->response = array(
            'result' => $products2 //возвращаем в ответ на AJAX-запрос массив продуктов данной категории
        );
    }
}